from message import Message
from random import randint


class Request(Message):
	'''
	Class to handle requests from both Client and Server side.
	'''
	def __init__(self, *args, **kwargs):
		super().__init__()
		self.ver	= kwargs.get('version', 1)
		self.type	= kwargs.get('type', 1)
		self.TKL	= kwargs.get('TKL', 8)
		self.code	= kwargs.get('code', 0)
		self.MID	= kwargs.get('MID', randint(0, 65536))
		self.token	= kwargs.get('token', randint(0, pow(2, 32)))
		self.payload = kwargs.get('data', None)

	def encode(self):
		'''
		This method will first create a header from the request's fields,  pack
		them with it's payload (if any) in the form of a message and encode it.
		'''
		header 	= f'{self.ver:02b}' + '/' + f'{self.type:02b}' + '/' + f'{self.TKL:04b}' + '/' + f'{self.code:08b}' + '/' + f'{self.MID:016b}' + '/' + f'{self.token:032b}'
		
		if self.payload is not None:
			if isinstance(self.payload, tuple):
				payload = str(self.payload[0]) + '/' + str(self.payload[1])
			else:
				payload = str(self.payload)

			message = str(header) + '/' + str(payload)
		else:
			message = str(header)

		return message.encode('utf-8')

	def decode(self, data):
		message = data.decode('utf-8')
		message = message.split('/')
		return message

	def unpack(self, data):
		'''
		Unpack the recived data after it has been decoded and form
		a mirror Request object to store the data for later use.
		'''
		message = self.decode(data)
		self.ver	= int(message[0], 2)
		self.T		= int(message[1], 2)
		self.TKL	= int(message[2], 2)
		self.code	= int(message[3], 2)
		self.MID	= int(message[4], 2)
		self.token	= int(message[5], 2)
		self.payload = (message[6], message[7])