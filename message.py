import collections
'''
Dictionary for message types.
'''
Types = {
	'CON': 0,		# Message is of type Confirmable (0)
	'NON': 1,		# Message is of type Non-confirmable (1)
	'ACK': 2,		# Message is of type Acknowledgement (2)
	'RST': 3,		# Message is of type Reset (3)
	'None': None 	# Message has not type.
}

'''
Dictionary for CoAP method codes.
NOTE! For the moment only GET and POST methods are implemented.
NOTE! Codes for the methods do not correspond to those in the RFC 7252.
'''
Codes = {
	'EMPTY': 0,
	'GET'  : 1,
	'POST' : 2,
	'CONVERT': 3
}


class Message(object):
	'''
	Class to handle messages.
	'''
	def __init__(self):
		'''
		Data structure that represent a CoAP message.
        '''
		self._ver	= 1
		self._type	= None
		self._TKL	= None
		self._code	= None
		self._MID	= None
		self._token 	= None
		self._option 	= None
		self._payload 	= None

	@property
	def ver(self):
		'''
		Version (ver):
		Indicates the CoAP version number. This field MUST remain to 1 (01 binary)
		as other values are reserved for further versions. Messages with unkown
		versions number will be silently ignored.

		return: 2-bit unsigned integer
		'''
		return self._ver
	
	@ver.setter
	def ver(self, value):
		'''
		raise AttributeError: if value set is not 1.
		'''
		if value == 1:
			self._ver = value
		else:
			raise AttributeError(value)

	@property
	def type(self):
		'''
		Type:
		Indicates if this message is of type Confirmable (0), Non-confirmable (1),
		Acknowledgement (2) or Reset (3).

		return: 2-bit unsigned integer.
		'''
		return self._type
	
	@type.setter
	def type(self, value):
		'''
		raise AttributeError: if the specified value does not exists among
		defined types (check message.Types for valid values).
		'''
		if value not in Types.values():
			raise AttributeError(value)
		self._type = value

	@property
	def TKL(self):
		'''
		Token Length (TKL):
		Indicates the length of the variable-length Token field(0-8 bytes). Lengths
		9-15 are reserved for message format error.

		return: 4-bit unsigned integer.
		'''
		return self._TKL

	@TKL.setter
	def TKL(self, value):
		if value not in range(0, 9):
			raise AttributeError(value)
		self._TKL = value
	
	@property
	def code(self):
		'''
		Code:
		Indicates either a request (0), a success response (2), a client error
		response (4), or a server error response (5).

		NOTE! As a special case, Code 0.00 indicates an Empty message.

		return: 8-bit unsigned integer.
		'''
		return self._code
	
	@code.setter
	def code(self, value):
		if value not in Codes.values() and value is not None:
			raise AttributeError
		self._code = value

	@property
	def MID(self):
		'''
		Message ID (MID):
		Used to detect message duplication and to match messages of type
		Acknowledgement/Reset to messages of type Confirmable/Non-confirmable.

		return: 16-bit unsigned integer.
      	'''
		return self._MID
	
	@MID.setter
	def MID(self, value):
		if value not in range(0, 65536):
			raise AttributeError(value)
		self._MID = value

	@property
	def token(self):
		'''
		Token:
		Is a value used to correlate requests and responses.
		
		return: 0-8 bytes depending on the TKL.
		'''
		return self._token
	
	@token.setter
	def token(self, value):
		if value not in range(0, 4294967296):
			raise AttributeError(value)
		self._token = value

	@property
	def option(self):
		'''
		Option:
		This will remain undealt with for now.

		return: ???
		'''
		return self._option
	
	@option.setter
	def option(self, value):
		self._option = value

	@property
	def payload(self):
		'''
		Payload:
		Used to send data and it's content type either from the server to
		client and vice versa.

		return: string
		'''
		return self._payload

	@payload.setter
	def payload(self, value):
		'''
		Logic behind using tuple:
			with got a target and a resource, as a client we want to pull
			target's resource from the server.

		e.g. ('Iasi', 'Temperature') would be sth like:
			request: From 'Iasi' GET 'Temperature'
			response: b'7.5 C'
		'''
		if isinstance(value, tuple):
			target, resource = value
			self._payload = (target, resource)
		else:
			self._payload = value

	def pretty_print(self):
		print("--------------------------")

		if self._ver is not None:
			print("CoAP version: " + f'{self._ver:02b}')
		else:
			print("CoAP version:", self._ver)

		if self._type is not None:
			print("Message type: " + f'{self._type:02b}')
		else:
			print("Message type:", self._type)

		if self._TKL is not None:
			print("Token length: " + f'{self._TKL:04b}')
		else:
			print("Token length:", self._TKL)

		if self._code is not None:
			print("Req/Res code: " + f'{self._code:08b}')
		else:
			print("Req/Res code:", self._code)

		if self._MID is not None:
			print("Message  ID: 0x" + format(self._MID, '04x').upper())
		else:
			print("Message  ID:", self._MID)
		
		if self._token is not None:
			print("Token value: 0x" + format(self._token, '08x').upper())
		else:
			print("Token value:", self._token)

		if self._option is not None:
			print("Options:", bytes(self._option))
		else:
			print("Options:", self._option)

		if self._payload is not None:
			print("Payload:", bytes(str(self._payload).encode('utf-8')))
		else:
			print("Payload:", self._payload)
		print("--------------------------")