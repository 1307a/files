import socket
from request import Request

class Client:

	def __init__(self, *args, **kwargs):
		self._host = kwargs.get('host', '127.0.0.1')
		self._port = kwargs.get('port', 5000)
		self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server = (self._host, self._port)
		self.__socket.connect(server)
		self.req_msg = None

	def request(self, T, code, data = None):
		self.req_msg = Request(type = T, code = code, data = data)
		self.req_msg.pretty_print()

		self.__socket.sendall(self.req_msg.encode())
		self._data = self.__socket.recv(1024)

	def close(self):
		self.__socket.sendall(b'exit')
		self.__socket.close()

	@property
	def host(self):
		return self._host

	@host.setter
	def host(self, value):
		if not isinstate(value, str):
			raise AttributeError(value)
		host = value

	@property
	def port(self):
		return self._port
	
	@port.setter
	def port(self, value):
		if value <= 1024 or not isinstate(value, int):
			raise AttributeError(value)
		port = value

	@property
	def data(self):
		return self._data