from titleBar import TitleBar
from tkinter import *
from extensions import *
from client import Client
import datetime, threading, random, queue, sys


class MainPanel(Canvas):

	CITIES		= ['Bacau', 'Galati', 'Iasi', 'Piatra Neamt', 'Suceava']
	RESOURCES	= ['Temperature', 'Feels Like', 'Min Temperature', 'Max Temperature', 'Pressure', 'Humidity']
	METHODS		= ['GET', 'POST', 'CONVERT']

	connected = False

	def __init__(self, master, hook, queue):
		super().__init__(master)
		self.place(width = 730, height = 565, x = 0, y = 0)
		self.config(bg = '#efefef', bd = 0)
		self.create_line(0, 52, 730, 52, fill = '#cacaca')
		self.queue = queue
		self.hook  = hook
		self.client = None

		# Terminal:
		terminal = Terminal(master)
		terminal.place(width = 700, x = 15, y = 273)
		self.output = TerminalOutput(terminal, 670, 232)

		# Labels:
		label_0 = CasualLabel(master, "Host", 92, 65)
		label_1 = CasualLabel(master, "Port", 95, 97)
		label_2 = CasualLabel(master, "City", 98, 127)
		label_3 = CasualLabel(master, "Resource", 60, 158)
		label_4 = CasualLabel(master, "Request Method", 10, 188)
		label_5 = CasualLabel(master, "Output", 10, 248)

		# InputBoxes:
		self.host_field = InputBox(master, "127.0.0.1", 135, 65, 298)
		self.port_field = InputBox(master, "5000", 135, 97, 101)
		self.city_field = InputComboBox(master, self.CITIES, self.CITIES[2], 135, 129, 253)
		self.res_field 	= InputComboBox(master,	self.RESOURCES, self.RESOURCES[0], 135, 158, 253)
		self.req_field 	= InputComboBox(master,	self.METHODS, self.METHODS[0], 135, 189, 151)

		self.res_field.config(state = 'readonly')
		self.req_field.config(state = 'readonly')

		# Buttons:
		self.obs_btn 	= UtilityButton(master, "Observe", 396, 158, 80)
		self.run_btn 	= UtilityButton(master, "Run", 135, 221, 80)
		self.start_btn  = UtilityButton(master, "Start Client", 223, 221, 116)
		self.addO_btn 	= UtilityButton(master, "Add Options", 412, 189, 95)
		self.addC_btn 	= UtilityButton(master, "Add Content", 515, 189, 95)
		
		self.obs_btn.config(state = DISABLED)
		self.addO_btn.config(state = DISABLED)
		self.addC_btn.config(state = DISABLED)
		self.run_btn.config(state = DISABLED, command = hook.action)
		self.start_btn.config(command = self.start_client)

		# Checkbox:
		self.chkValue = IntVar()
		self.confirm_btn = Checkbutton(master, text = "Confirmable", var = self.chkValue)
		self.confirm_btn.place(x = 288, y = 189, width = 116, height = 25)
		self.confirm_btn.config(font = ('Ubuntu Regular', 11))

	def update(self):
		while self.queue.qsize():
			try:
				msg = self.queue.get(0)
				self.output.insertData(msg)
			except queue.Empty:
				pass

	def start_client(self):
		host = self.host_field.get(1.0, END).rstrip()
		port = int(self.port_field.get(1.0, END).rstrip())
		
		try:
			self.client = Client(host = host, port = port)
			self.run_btn.config(state = NORMAL)
			self.start_btn.config(text = 'Stop Client', command = self.stop_client)
			self.host_field.config(state = DISABLED)
			self.port_field.config(state = DISABLED)
			self.connected = True
		except:
			time = datetime.datetime.now()
			time = '[' + str(time.hour) + ':' + str(time.minute) + ':' + str(time.second) + '] '
			err  = time + '[from Client]:   Connection to server could not be established...'
			self.hook.action(err)

	def stop_client(self):
		self.client.close()
		self.run_btn.config(state = DISABLED)
		self.host_field.config(state = NORMAL)
		self.port_field.config(state = NORMAL)
		self.start_btn.config(text = 'Start Client', command = self.start_client)
		self.connected = False

	def send_request(self):
		'''
		Read and store input data for local use.
		'''
		host = self.host_field.get(1.0, END).rstrip()
		port = int(self.port_field.get(1.0, END).rstrip())
		city = self.city_field.get()
		resource = self.res_field.current()

		if self.chkValue.get() == 0:
			T = 1
		else:
			T = 0
		
		method = self.req_field.current() + 1
		time = datetime.datetime.now()
		time = '[' + str(time.hour) + ':' + str(time.minute) + ':' + str(time.second) + '] '

		try:
			'''
			Try to form a request based on the input data.
			- client.request(type, method, payload)
			NOTE: payload can be a tuple with (target, resource)
			'''
			self.client.request(T, method, (city, resource))
			return time + '[from Server]:   ' + self.client.data.decode('utf-8')

		except:
			return time + '[from Client]:   Request did not reach the server...'		
		


class GUIThread:

	def __init__(self, master):
		self.master = master
		self.data 	= None
		self.queue 	= queue.Queue()
		self.mainPanel = MainPanel(master, self, self.queue)
		TitleBar(master, "CoAP Client", self.stop)
		
	def action(self, err = None):
		if self.mainPanel.connected:
			self.data = self.mainPanel.send_request()
		else:
			self.data = err

	def start(self):
		self.running = True
		self.thread  = threading.Thread(target = self.run)
		self.thread.start()
		self.update()

	def update(self):
		'''
		Every 0.1 seconds check if still running and update if necessary.
		'''
		if not self.running:
			'''
			If not running, exit.
			'''
			sys.exit(1)

		self.mainPanel.update()
		self.master.after(100, self.update)

	def run(self):
		while self.running:
			if self.data:
				self.queue.put(self.data)
				self.data = None

	def stop(self):
		self.running = False


class Window(Tk):

	def __init__(self):
		super().__init__()

		self.config(bg = '#efefef')
		self.geometry('730x565')
		self.overrideredirect(True)
		self.center()

	def start(self):
		GUIThread(self).start()
		self.mainloop()

	def center(self):
		self.update_idletasks()
		width  = self.winfo_width()
		height = self.winfo_height()
		x = (self.winfo_screenwidth() // 2) - (width // 2)
		y = (self.winfo_screenheight() // 2) - (height // 2)
		self.geometry('{}x{}+{}+{}'.format(width, height, x, y))


if __name__ == '__main__':
	Window().start()