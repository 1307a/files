from tkinter.ttk import Combobox
from tkinter import *
import pyglet, datetime


pyglet.font.add_file('Ubuntu-Regular.ttf')
ubuntu_regular = pyglet.font.load('Ubuntu Regular')
time = datetime.datetime.now()


class Terminal(Canvas):

	def __init__(self, master):
		super().__init__(master)
		self.config(highlightbackground = '#cbcbcb', highlightcolor = '#cbcbcb', highlightthickness = 1)
		self.config(bg = '#ececec')


class TerminalOutput(Text):

	def __init__(self, master, w, h):
		super().__init__(master)
		self.config(highlightbackground = 'grey73', highlightcolor = 'grey73', highlightthickness = 1, bd = 0)
		self.config(padx = 4, pady = 1, font = ('Ubuntu Regular', 11))
		self.insert(END, "----------- Session: " + time.strftime("%d-%m-%Y %H:%M") + " ---------------------------------------------------------------------\n")
		self.place(x = 15, y = 15, width = w, height = h)
		self.config(state = DISABLED)

	def insertData(self, data):
		if data is not None:
			self.config(state = NORMAL)
			self.insert(END, str(data) + '\n')
			self.config(state = DISABLED)


class CasualLabel(Label):

	def __init__(self, master, title, posX, posY):
		super().__init__(master)
		self.posX = posX
		self.posY = posY
		self.place(x = posX, y = posY)
		self.config(text = title, font = ('Ubuntu Regular', 11))


class InputBox(Text):

	def __init__(self, master, value, posX, posY, w, h = None):
		super().__init__(master)
		self.config(highlightbackground = 'grey73', highlightcolor = 'grey73', highlightthickness = 1, bd = 0)
		self.config(padx = 2, pady = 2, height = 1, font = ('Ubuntu Regular', 11))
		self.insert(END, value)
		self.place(x = posX, y = posY, width = w, height = h)


class InputComboBox(Combobox):

	def __init__(self, master, vList, value, posX, posY, w, h = None):
		super().__init__(master, value = vList)
		self.config(font = ('Ubuntu Regular', 11))
		self.set(value)
		self.place(x = posX, y = posY, width = w, height = h)


class UtilityButton(Button):

	def __init__(self, master, name, posX, posY, w, h = 25):
		super().__init__(master)
		self.place(x = posX, y = posY, width = w, height = h)
		self.config(font = ('Ubuntu Regular', 11), text = name, bd = 1)