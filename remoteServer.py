import socket, json, sys


class RemoteServer:
	'''
	Class to handle connection to remote server.
	'''
	ALLOWED_METHODS = {
		1:	'GET',
		2:	'POST',
	}

	DISCOVERY_PATH = '/data/2.5/weather?q='
	URI_PATH = '&APPID=8a999283783fff58a906c31b2e47a26d&units=metric HTTP/1.1\r\nHost: api.openweathermap.org\r\n\r\n'

	def __init__(self, host, port):
		'''
		Open socket on IPv4 over TCP.
		'''
		self._host = host
		self._port = port
		self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def connect(self, client):
		'''
		Establish connection with the client (in this case our CoAP Server).
		'''
		try:
			'''
			Set a connection timeout of 4 sec. If the client doesn't manage
			to connect after 4 sec, the connection will be closed and an
			error message will be sent to the client.
			'''
			socket.setdefaulttimeout(4)
			self._socket.connect((self._host, self._port))
		except:
			conn_error = 'Connection to the weather server failed...'
			client.sendall(conn_error.encode('utf-8'))
			sys.exit()

	def pullData(self, request):
		'''
		request[0]: MUST contain the method,
		request[1]: MUST contain the target,
		request[2]: MUST contain the wanted resource from specified target.

		NOTE! It is user's duty to check that the requirements are met.
		'''
		if request[0] == 3:
			'''
			CONVERT method will be seen as a POST method to update the existing data.
			CONVERT method has code: 3, see message.py CODES.
			'''
			command = self.ALLOWED_METHODS.get(2) + ' ' + self.DISCOVERY_PATH + str(request[1]) + self.URI_PATH
		else:
			command = self.ALLOWED_METHODS.get(request[0]) + ' ' + self.DISCOVERY_PATH + str(request[1]) + self.URI_PATH
		
		self._socket.sendall(command.encode('utf-8'))
		response = self._socket.recv(5000).decode('utf-8')
		data = json.loads(response[response.rfind('\n'):])

		resource = {
			0: str(data['main']['temp']) + '  (Celsius)',
			1: str(data['main']['feels_like']) + '  (Celsius)',
			2: str(data['main']['temp_min']) + '  (Celsius)',
			3: str(data['main']['temp_max']) + '  (Celsius)',
			4: str(data['main']['pressure']) + '  (mBar)',
			5: str(data['main']['humidity']) + '  (%)',
		}

		converted_res = {
			0: str(data['main']['temp'] * 1.8 + 32) + '  (Fahrenheit)',
			1: str(data['main']['feels_like'] * 1.8 + 32) + '  (Fahrenheit)',
			2: str(data['main']['temp_min'] * 1.8 + 32) + '  (Fahrenheit)',
			3: str(data['main']['temp_max'] * 1.8 + 32) + '  (Fahrenheit)',
			4: str(data['main']['pressure'] * 0.000987 ) + '  (atm)',
			5: str(data['main']['humidity'] // 100),
		}

		if request[0] == 3:
			return converted_res.get(int(request[2]), 'Non-existing resource...')
		return resource.get(int(request[2]), 'Non-existing resource...')