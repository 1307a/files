from tkinter import *
from extensions import ubuntu_regular

class ExitButton(Button):

	def __init__(self, master, exitCommand):
		super().__init__(master)
		self.master = master
		self.pack(side = RIGHT)
		self.config(fg = 'white', font = ("Sans Serif", 15), text = '\u00D7', command = exitCommand, bd = 0, bg = '#2b2929')
		self.bind("<Enter>", self.on_enter)
		self.bind("<Leave>", self.on_leave)

	def on_enter(btn, event):
		btn.config(bg = '#ff0000')

	def on_leave(btn, event):
		btn.config(bg = '#2b2929')


class MinimizeButton(Button):

	def __init__(self, master):
		super().__init__(master)
		self.master = master
		self.pack(side = RIGHT)
		self.config(state = DISABLED, font = ("Sans Serif", 14), text = '\u2212', command = master.destroy, bd = 0, bg = '#2b2929')

class MaximizeButton(Button):

	def __init__(self, master):
		super().__init__(master)
		self.master = master
		self.pack(side = RIGHT)
		self.config(state = DISABLED, padx = 10, font = ("Sans Serif", 14), text = '\u25A1', command = master.destroy, bd = 0, bg = '#2b2929')


class TitleBar(Frame):

	def __init__(self, master, title, exitCommand):
		super().__init__(master)
		self.master = master
		self.title = title
		self.exitCommand = exitCommand
		self.posX = 0
		self.posY = 0
		self.grid()
		self.draw()
		self.bind("<ButtonPress-1>", self.StartMove)
		self.bind("<B1-Motion>", self.OnMotion)

	def draw(self):
		self.config(bg = '#2b2929', height = 37)
		self.place(x = 0, y = 0)
		self.pack(fill = 'x')
		
		title = Label(self, text = self.title)
		exit = ExitButton(self.master, self.exitCommand)
		maximize = MaximizeButton(self.master)
		minimize = MinimizeButton(self.master)

		title.config(bg = '#2b2929', fg = 'white', font = ('Ubuntu Regular', 11))
		title.place(relx = 0.57, rely = 0, height = 38, anchor = NE)
		exit.place(relx = 1.0, rely = 0, height = 37, width = 37, anchor = NE)
		maximize.place(relx = 0.947, y = -3, height = 37, width = 37, anchor = NE)
		minimize.place(relx = 0.9, rely = 0, height = 37, width = 37, anchor = NE)


	def StartMove(self, event):
		self.posX = event.x
		self.posY = event.y

	def OnMotion(self, event):
		x = self.master.winfo_pointerx() - self.posX
		y = self.master.winfo_pointery() - self.posY
		self.master.geometry('+{x}+{y}'.format(x = x, y = y))