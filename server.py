from threading import Thread
from request import Request
from remoteServer import RemoteServer
import socket, queue, time, select


requests_queue = queue.PriorityQueue(20)


class Command:

	client_req = Request()

	def __init__(self, request, conn):
		self._conn = conn
		self.client_req.unpack(request)

		org_server = RemoteServer('api.openweathermap.org', 80)
		org_server.connect(conn)
		org_data = org_server.pullData((self.client_req.code, self.client_req.payload[0], self.client_req.payload[1]))

		self._data = self.genMess(org_data)

	def genMess(self, payload):
		message = ''

		if self.client_req.T == 0:
			message += 'ACK'
		message += ' ' + '[0x' + str(format(self.client_req.MID, '04x').upper()) + ']'
		message += ' ' + '(token 0x' + str(format(self.client_req.token, '08x').upper()) + ')'
		if self.client_req.code == 1:
			message += ' ' + '2.05 (Content)'
		elif self.client_req.code == 2:
			message += ' ' + '2.04 (Changed)'
		elif self.client_req.code == 3:
			message += ' ' + '2.04 (Changed)'
		message += ' ' +  str(payload)

		return message

	@property
	def data(self):
		return self._data

	@property
	def conn(self):
		return self._conn


class RequestThread(Thread):

	running = False

	def __init__(self, *args, **kwargs):
		Thread.__init__(self)

	def run(self):
		while self.running:
			if not requests_queue.empty():
				command = requests_queue.get()
				command.conn.send(command.data.encode('utf-8'))


class ClientThread(Thread):

	def __init__(self, conn, addr):
		Thread.__init__(self)
		self._conn = conn
		self._host = addr[0]
		self._port = addr[1]

		print('[+] New client thread for connection ' + self._host + ':' + str(self._port))

	def run(self):

		while True:
			ready, _, _ = select.select([self._conn], [], [], 60)
			if not ready:
				print('[!] No data recived. Connection timed out...')
				break
			else:
				try:
					data = self._conn.recv(1024)
					print('From [' + self._host + ':' + str(self._port) + ']:', data)
				except:
					print('[-] Client thread ended for connection ' + self._host + ':' + str(self._port))
					break

			if data == b'quit' or data == b'exit':
				print('[-] Client thread ended for connection ' + self._host + ':' + str(self._port))
				break

			if not requests_queue.full():
				requests_queue.put(Command(data, self._conn))


class Server:

	running = False

	def __init__(self, *args, **kwargs):
		self._host = kwargs.get('host', '0.0.0.0')
		self._port = kwargs.get('port', 5000)
		self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self._socket.settimeout(60)
		self._socket.timeout
		self._socket.bind((self._host, self._port))
		self._threads = []
		self._running = False

	def start(self):
		self.running = True
		self.command = RequestThread()
		self.command.running = True
		self.command.start()
		print('------- Multithreaded Python CoAP (ver 1.0) Server -----------------------')
		

		while self.running:
			try:
				self._threads.append(self.command)
				self._socket.listen()
				print('[?] Waiting for connections...')

				conn, addr = self._socket.accept()
				new_client_thread = ClientThread(conn, addr)
				new_client_thread.start()
				self._threads.append(new_client_thread)
			except:
				print('[!] Connection to server timed out...')
				self.command.running = False
				self._socket.close()
				self._running = False
				break

		for x in self._threads:
			x.join()


if __name__ == '__main__':
	Server().start()